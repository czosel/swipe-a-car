// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', [
  'ionic',

  'starter.directives',

  'starter.config',

  'starter.controllers',
  'starter.indexController',
  'starter.index2Controller',
  'starter.detailController',
  'starter.likeController',

  'starter.galleryController',

  'starter.queryService',

  'starter.favoriteService',
  'starter.recommenderService',

  'ionic.contrib.ui.tinderCards',

  'starter.fueltypeFilter'

])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: 'templates/menu.html',
      controller: 'AppCtrl'
    })

  .state('app.index', {
    url: '/welcome',
    views: {
      'menuContent': {
        templateUrl: 'templates/index.html',
        controller: 'IndexCtrl'
      }
    }
  })

  .state('app.index2', {
    url: '/info',
    views: {
      'menuContent': {
        templateUrl: 'templates/index2.html',
        controller: 'Index2Ctrl'
      }
    }
  })

  .state('app.swipe', {
    url: '/swipe',
    views: {
      'menuContent': {
        templateUrl: 'templates/swipe.html',
        controller: 'SwipeCtrl'
      }
    }
  })
  .state('app.likes', {
    url: '/likes',
    views: {
      'menuContent': {
        templateUrl: 'templates/likes.html',
        controller: 'LikesCtrl'
      }
    }
  })
  .state('app.detail', {
    url: '/detail/:vin',
    views: {
      'menuContent': {
        templateUrl: 'templates/detail.html',
        controller: 'DetailCtrl'
      }
    }
  })

  .state('app.gallery', {
    url: '/gallery/:vin/:key',
    views: {
      'menuContent': {
        templateUrl: 'templates/gallery.html',
        controller: 'GalleryCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/welcome');
});
