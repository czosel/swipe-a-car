angular.module('starter.config', [])
  .constant('config', {
    price: {
      mutationFactor: 30000,
      operator: "range"
    },
    mileage: {
      mutationFactor: 100000,
      operator: "range"
    },
    modelYear: {
      mutationFactor: 12,
      operator: "range"
    },
    sportScore: {
      mutationFactor: 2,
      operator: "range"
    },
    familyScore: {
      mutationFactor: 2,
      operator: "range"
    },
    ecoScore: {
      mutationFactor: 2,
      operator: "range"
    },
    priceScore: {
      mutationFactor: 2,
      operator: "range"
    },
    offroadScore: {
      mutationFactor: 2,
      operator: "range"
    },
    designScore: {
      mutationFactor: 2,
      operator: "range"
    },
    brand: {
      mutationFactor: 1,
      operator: "contains"
    },
    fuelType:{
      mutationFactor: 1,
      operator: "contains"
    }
  }).constant("queryOperators",{
    range : function (data){
      var p_min = Math.round((data.value - (data.factor * data.curSigma)/2)*100)/100;
      var p_max = Math.round((data.value + (data.factor * data.curSigma)/2)*100)/100;
      return {
        min: p_min,
        max: p_max
      };
    },
    contains : function (data){
      if(data.curSigma > 0.8){
        return false;
      }
      var p_string = data.value;
      return {'contains': p_string};
    }
  });
