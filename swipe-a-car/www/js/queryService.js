angular.module('starter.queryService', [])
  .factory('queryService', function($http, $state) {
    var filepath = "assets/vehicle.json";
    var data = null,
      rawData = null;

    function redirectIfNotInitialized() {
      if (data === null) {
        $state.go('app.index');
      }
    }

    return {
      init: function() {
        return $http.get(filepath).success(function(response) {
          console.log('query service loaded data');
          rawData = response.vehicles;
          data = Defiant.getSnapshot(response);
        });
      },
      hasData: function() {
        return data !== null;
      },
      getRandom: function() {
        redirectIfNotInitialized();
        if(rawData === null)return null;
        var key = Math.floor(Math.random() * rawData.length);
        return rawData[key];
      },
      fetchRowset: function(searchObj) {
        redirectIfNotInitialized();

        var searchStr = "//vehicles";
        Object.keys(searchObj).forEach(function(attribute) {
          Object.keys(searchObj[attribute]).forEach(function(operator) {
            switch (operator) {
              case 'min':
                searchStr += "[" + attribute + " > " + searchObj[attribute][operator] + "]";
                break;
              case 'max':
                searchStr += "[" + attribute + " < " + searchObj[attribute][operator] + "]";
                break;
              case 'contains':
                searchStr += "[contains(" + attribute + ",'" + searchObj[attribute][operator] + "')]";
                break;
            }
          });
        });

        found = JSON.search(data, searchStr);
        return found;
      }
    };
  });
