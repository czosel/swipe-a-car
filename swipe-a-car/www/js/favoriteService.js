angular.module('starter.favoriteService', [])
  .factory('favoriteService', function () {
    var toplist = [];
    var likes = [];
    var numberOfCarsInToplist = 10;

    function isCarInList(list, car){
      return list.indexOf(car) !== -1;
    }

    return {
      addCar : function (car){
        if(toplist.length == numberOfCarsInToplist){
            toplist.splice(0, 1);
        }
        if(this.isCarInToplist(car) === false){
          toplist.push(car);
        }
        if(this.isCarInLikes(car) === false){
          likes.push(car);
        }

      },
      getToplist : function (){
        return toplist;
      },
      getAllLikes: function (){
        return likes;
      },
      clearHistory: function (){
        toplist = [];
        likes = [];
      },
      isCarInLikes: function (car){
        return isCarInList(likes, car);
      },
      isCarInToplist: function (car){
        return isCarInList(toplist, car);
      }
    };
  });
