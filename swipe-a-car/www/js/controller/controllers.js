angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $rootScope, favoriteService) {
  $scope.favoriteService = favoriteService;
  $scope.clearLikeHistory = function(){
      favoriteService.clearHistory();
      $rootScope.isInit = true;
  };
})

.controller('SwipeCtrl', function($scope, $rootScope, $state, queryService, favoriteService, recommenderService, $ionicPopup) {
  $scope.cards = [];

  function initRandomCars(amount) {
    for (var i = 0; i < amount; i++) {
      var car = queryService.getRandom();
      if (car === null) continue;
      addCarToCards(car);
    }
  }

  function addCarToCards(car) {
    if ($scope.cards.indexOf(car) !== -1) {
      var cars = recommenderService.getRecommendations(1);
      addCarToCards(cars[0]);
    } else {
      $scope.cards.unshift(car);
    }
  }

  // An alert dialog
  var showAlert = function() {
    var alertPopup = $ionicPopup.alert({
      title: 'Kennenlernen abgeschlossen',
      template: 'Du kannst dir jetzt in Ruhe deine persönlichen Vorschläge anschauen. Deine persönlichen Top 10 sind über das Stern-Icon erreichbar. Tipp: Je länger du swipest, umso besser werden die Vorschläge!'
    });
    alertPopup.then(function(res) {
      console.log('Thank you for not eating my delicious ice cream cone');
    });
  };

  $scope.$on('$ionicView.enter', function(e) {

    $rootScope.isInit = !favoriteService.getToplist() || favoriteService.getToplist().length < 10;
    if ($scope.cards.length === 0) {
      initRandomCars(10);
    }
    //$scope.cards = favoriteService.getToplist(); //TODO

    function removeTopCard() {
      $scope.cards.splice(-1, 1);
      if ($scope.cards.length <= 5) {
        if (favoriteService.getToplist().length < 10) {
          initRandomCars(5);
        } else {
          newCars = recommenderService.getRecommendations(5);
          newCars.forEach(function(car) {
            addCarToCards(car);
          });
        }
      }
    }

    $scope.cardDestroyed = function() {
      removeTopCard();
    };

    $scope.cardTransitionRight = function() {
      var car = $scope.cards[$scope.cards.length - 1];
      favoriteService.addCar(car);
      if ($rootScope.isInit && favoriteService.getToplist().length === 10) {
        $rootScope.isInit = false;
        showAlert();
      }
    };

    $scope.openDetail = function() {
      $state.go('app.detail', {
        vin: $scope.cards[$scope.cards.length - 1].vin
      });
    };
  });
})

.controller('CardCtrl', function($scope, TDCardDelegate) {

});
