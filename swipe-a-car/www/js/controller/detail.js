angular.module('starter.detailController', [])

.controller('DetailCtrl', function($scope, $state, $stateParams, queryService) {
  $scope.$on('$ionicView.enter', function(e) {

    $scope.car = queryService.fetchRowset({
      vin: {
        contains: $stateParams.vin
      }
    })[0];

    $scope.showImage = function(vin, key) {
      $state.go('app.gallery', {vin: vin, key: key});
    };
  });
});
