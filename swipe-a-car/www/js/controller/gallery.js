angular.module('starter.galleryController', [])

.controller('GalleryCtrl', function($scope, $stateParams, queryService) {
  console.log('gallery', $stateParams);
  $scope.$on('$ionicView.enter', function(e) {
    $scope.key = $stateParams.key;
    $scope.car = queryService.fetchRowset({
      vin: {
        contains: $stateParams.vin
      }
    })[0];
    console.log($scope.car);

    $scope.slideHasChanged = function(index) {
      $scope.key = index;
    };
  });
});
