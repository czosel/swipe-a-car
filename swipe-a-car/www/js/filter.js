angular.module('starter.fueltypeFilter', [])

.filter('fueltype', function() {
  return function(type) {
    var typeCfg = {
      B : "Benzin",
      D : "Diesel",
      E : "Elektrisch",
      G : "Gas",
      PH : "Plugin-Hybrid"
    };
    return typeCfg[type];
  };
}).constant("gearTypes",[{
    "id": "GE000001",
    "category": "Manual",
    "drive": "FWD",
    "nameGerman": "5-Gang manuell Frontantrieb",
    "gearCount": 5
  }, {
    "id": "GE000002",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "4-Gang Automat Frontantrieb",
    "gearCount": 4
  }, {
    "id": "GE000003",
    "category": "Manual",
    "drive": "AWD",
    "nameGerman": "5-Gang manuell quattro",
    "gearCount": 5
  }, {
    "id": "GE000004",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "5-Gang Tiptronic Frontantrieb",
    "gearCount": 5
  }, {
    "id": "GE000005",
    "category": "Automatic",
    "drive": "AWD",
    "nameGerman": "5-Gang Tiptronic quattro",
    "gearCount": 5
  }, {
    "id": "GE000006",
    "category": "Manual",
    "drive": "FWD",
    "nameGerman": "6-Gang manuell Frontantrieb",
    "gearCount": 6
  }, {
    "id": "GE000007",
    "category": "Manual",
    "drive": "AWD",
    "nameGerman": "6-Gang manuell quattro",
    "gearCount": 6
  }, {
    "id": "GE000008",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "6-Gang Multitronic Frontantrieb",
    "gearCount": 6
  }, {
    "id": "GE000009",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "5-Gang Direkt-Schaltgetriebe DSG Frontantrieb",
    "gearCount": 5
  }, {
    "id": "GE000010",
    "category": "Manual",
    "drive": "FWD",
    "nameGerman": "6-Gang Low Range quattro",
    "gearCount": 6
  }, {
    "id": "GE000011",
    "category": "Manual",
    "drive": "FWD",
    "nameGerman": "5-Gang manuell Frontantrieb",
    "gearCount": 5
  }, {
    "id": "GE000012",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "5-Gang Tiptronic Frontantrieb",
    "gearCount": 5
  }, {
    "id": "GE000014",
    "category": "Manual",
    "drive": "AWD",
    "nameGerman": "6-Gang manuell Total Traction 4",
    "gearCount": 6
  }, {
    "id": "GE000015",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "4-Gang Automat Frontantrieb",
    "gearCount": 4
  }, {
    "id": "GE000016",
    "category": "Manual",
    "drive": "FWD",
    "nameGerman": "6-Gang manuell Frontantrieb",
    "gearCount": 6
  }, {
    "id": "GE000017",
    "category": "Manual",
    "drive": "FWD",
    "nameGerman": "5-Gang manuell Frontantrieb",
    "gearCount": 5
  }, {
    "id": "GE000018",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "4-Gang Automat Frontantrieb",
    "gearCount": 4
  }, {
    "id": "GE000019",
    "category": "Manual",
    "drive": "AWD",
    "nameGerman": "6-Gang manuell Allradantrieb",
    "gearCount": 6
  }, {
    "id": "GE000020",
    "category": "Manual",
    "drive": "AWD",
    "nameGerman": "5-Gang manuell Allradantrieb",
    "gearCount": 5
  }, {
    "id": "GE000021",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "5-Gang Tiptronic Frontantrieb",
    "gearCount": 5
  }, {
    "id": "GE000022",
    "category": "Manual",
    "drive": "FWD",
    "nameGerman": "6-Gang manuell Frontantrieb",
    "gearCount": 6
  }, {
    "id": "GE000023",
    "category": "Manual",
    "drive": "RWD",
    "nameGerman": "5-Gang Manuell Heckantrieb",
    "gearCount": 5
  }, {
    "id": "GE000024",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "4-Gang Automat Frontantrieb",
    "gearCount": 4
  }, {
    "id": "GE000025",
    "category": "Manual",
    "drive": "AWD",
    "nameGerman": "5-Gang Manuell 4Motion",
    "gearCount": 5
  }, {
    "id": "GE000026",
    "category": "Manual",
    "drive": "FWD",
    "nameGerman": "5-Gang Manuell Frontantrieb",
    "gearCount": 5
  }, {
    "id": "GE000030",
    "category": "Manual",
    "drive": "FWD",
    "nameGerman": "5-Gang manuell Frontantrieb",
    "gearCount": 5
  }, {
    "id": "GE000031",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "4-Gang Automat Frontantrieb",
    "gearCount": 4
  }, {
    "id": "GE000032",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "5-Gang Tiptronic Frontantrieb",
    "gearCount": 5
  }, {
    "id": "GE000034",
    "category": "Manual",
    "drive": "AWD",
    "nameGerman": "5-Gang manuell 4 MOTION",
    "gearCount": 5
  }, {
    "id": "GE000035",
    "category": "Manual",
    "drive": "AWD",
    "nameGerman": "6-Gang manuell 4 MOTION",
    "gearCount": 6
  }, {
    "id": "GE000037",
    "category": "Automatic",
    "drive": "AWD",
    "nameGerman": "5-Gang Tiptronic 4 MOTION",
    "gearCount": 5
  }, {
    "id": "GE000038",
    "category": "Manual",
    "drive": "FWD",
    "nameGerman": "6-Gang manuell Frontantrieb",
    "gearCount": 6
  }, {
    "id": "GE000039",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "5-Gang Automat DSG Frontantrieb",
    "gearCount": 5
  }, {
    "id": "GE000041",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "6-Gang Tiptronic Frontantrieb",
    "gearCount": 6
  }, {
    "id": "GE000042",
    "category": "Automatic",
    "drive": "AWD",
    "nameGerman": "6-Gang Tiptronic quattro",
    "gearCount": 6
  }, {
    "id": "GE000043",
    "category": "Automatic",
    "drive": "AWD",
    "nameGerman": "6-Gang Tiptronic 4XMOTION",
    "gearCount": 6
  }, {
    "id": "GE000052",
    "category": "Manual",
    "drive": "FWD",
    "nameGerman": "6-Gang Manuell Frontantrieb",
    "gearCount": 6
  }, {
    "id": "GE000062",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "6-Gang Tiptronic Frontantrieb",
    "gearCount": 6
  }, {
    "id": "GE000081",
    "category": "Manual",
    "drive": "AWD",
    "nameGerman": "6-Gang manuell 4XMOTION",
    "gearCount": 6
  }, {
    "id": "GE000092",
    "category": "Automatic",
    "drive": "AWD",
    "nameGerman": "6-Gang Tiptronic 4 MOTION",
    "gearCount": 6
  }, {
    "id": "GE000101",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "6-Gang Automat Frontantrieb",
    "gearCount": 6
  }, {
    "id": "GE000111",
    "category": "Automatic",
    "drive": "AWD",
    "nameGerman": "6-Gang Automat DSG 4MOTION",
    "gearCount": 6
  }, {
    "id": "GE000122",
    "category": "Automatic",
    "drive": "AWD",
    "nameGerman": "6-Gang Direkt-Schaltgetriebe DSG quattro",
    "gearCount": 6
  }, {
    "id": "GE000131",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "6-Gang Automat Frontantrieb",
    "gearCount": 6
  }, {
    "id": "GE000152",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "6-Gang Automat DSG Frontantrieb",
    "gearCount": 6
  }, {
    "id": "GE000161",
    "category": "Manual",
    "drive": "AWD",
    "nameGerman": "6-Gang Manuell 4Motion",
    "gearCount": 6
  }, {
    "id": "GE000171",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "6-Gang Direkt-Schaltgetriebe DSG Frontantrieb",
    "gearCount": 6
  }, {
    "id": "GE000181",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "6-Gang Tiptronic Frontantrieb",
    "gearCount": 6
  }, {
    "id": "GE000201",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "5-Gang Automatisiertes Schaltgetriebe",
    "gearCount": 5
  }, {
    "id": "GE000212",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "6-Gang DSG Frontantrieb",
    "gearCount": 6
  }, {
    "id": "GE000221",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "6-Gang DSG Frontantrieb",
    "gearCount": 6
  }, {
    "id": "GE000231",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "7-Gang Multitronic Frontantrieb",
    "gearCount": 7
  }, {
    "id": "GE000241",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "6-Gang Tiptronic Frontantrieb",
    "gearCount": 6
  }, {
    "id": "GE000251",
    "category": "Manual",
    "drive": "RWD",
    "nameGerman": "6-Gang Manuell Heckantrieb",
    "gearCount": 6
  }, {
    "id": "GE000281",
    "category": "Automatic",
    "drive": "RWD",
    "nameGerman": "6-Gang Automatisiertes Schaltgetriebe Heckantrieb",
    "gearCount": 6
  }, {
    "id": "GE000291",
    "category": "Automatic",
    "drive": "AWD",
    "nameGerman": "6-Gang Direkt-Schaltgetriebe S tronic quattro",
    "gearCount": 6
  }, {
    "id": "GE000292",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "6-Gang Direkt-Schaltgetriebe S tronic Frontantrieb",
    "gearCount": 6
  }, {
    "id": "GE000301",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "6-Gang Automat Frontantrieb",
    "gearCount": 6
  }, {
    "id": "GE000311",
    "category": "Automatic",
    "drive": "AWD",
    "nameGerman": "6-Gang R tronic quattro",
    "gearCount": 6
  }, {
    "id": "GE000321",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "7-Gang Automat DSG Frontantrieb",
    "gearCount": 7
  }, {
    "id": "GE000331",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "8-Gang Multitronic Frontantrieb",
    "gearCount": 8
  }, {
    "id": "GE000351",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "7-Gang Direkt-Schaltgetriebe S tronic Frontantrieb",
    "gearCount": 7
  }, {
    "id": "GE000361",
    "category": "Automatic",
    "drive": "AWD",
    "nameGerman": "7-Gang Direkt-Schaltgetriebe S tronic quattro",
    "gearCount": 7
  }, {
    "id": "GE000362",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "7-Gang DSG Frontantrieb",
    "gearCount": 7
  }, {
    "id": "GE000363",
    "category": "Automatic",
    "drive": "AWD",
    "nameGerman": "6-Gang DSG Allradantrieb",
    "gearCount": 6
  }, {
    "id": "GE000364",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "7-Gang DSG Frontantrieb",
    "gearCount": 7
  }, {
    "id": "GE000372",
    "category": "Manual",
    "drive": "AWD",
    "nameGerman": "6-Gang manuell 4x4",
    "gearCount": 6
  }, {
    "id": "GE000373",
    "category": "Automatic",
    "drive": "AWD",
    "nameGerman": "6-Gang DSG 4x4",
    "gearCount": 6
  }, {
    "id": "GE000396",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "7-Gang Automat DSG Frontantrieb",
    "gearCount": 7
  }, {
    "id": "GE000401",
    "category": "Automatic",
    "drive": "AWD",
    "nameGerman": "8-Gang Automat 4XMOTION",
    "gearCount": 8
  }, {
    "id": "GE000402",
    "category": "Automatic",
    "drive": "AWD",
    "nameGerman": "8-Gang Tiptronic quattro",
    "gearCount": 8
  }, {
    "id": "GE000404",
    "category": "Automatic",
    "drive": "AWD",
    "nameGerman": "7-Gang Automat DSG 4Motion",
    "gearCount": 7
  }, {
    "id": "GE000412",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "7-Gang Multitronic Frontantrieb",
    "gearCount": 7
  }, {
    "id": "GE000414",
    "category": "Automatic",
    "drive": "AWD",
    "nameGerman": "8-Gang Automat 4MOTION",
    "gearCount": 8
  }, {
    "id": "GE000422",
    "category": "Manual",
    "drive": "AWD",
    "nameGerman": "6-Gang Manuell 4Motion zuschaltbar",
    "gearCount": 6
  }, {
    "id": "GE000424",
    "category": "Manual",
    "drive": "AWD",
    "nameGerman": "6-Gang Manuell 4Motion permanent",
    "gearCount": 6
  }, {
    "id": "GE000425",
    "category": "Automatic",
    "drive": "AWD",
    "nameGerman": "7-Gang Automat DSG 4MOTION",
    "gearCount": 7
  }, {
    "id": "GE000441",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "6-Gang Automat DSG Frontantrieb",
    "gearCount": 6
  }, {
    "id": "GE000451",
    "category": "Automatic",
    "drive": "AWD",
    "nameGerman": "6-Gang Automat DSG 4Motion",
    "gearCount": 6
  }, {
    "id": "GE000471",
    "category": "Automatic",
    "drive": "AWD",
    "nameGerman": "8-Gang Automat 4Motion permanent",
    "gearCount": 8
  }, {
    "id": "GE000481",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "8-Gang Tiptronic Frontantrieb",
    "gearCount": 8
  }, {
    "id": "GE000491",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "5-Gang Automat Frontantrieb",
    "gearCount": 5
  }, {
    "id": "GE000501",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "5-Gang Automat Frontantrieb",
    "gearCount": 5
  }, {
    "id": "GE000511",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "5-Gang Automat Frontantrieb",
    "gearCount": 5
  }, {
    "id": "GE000521",
    "category": "Automatic",
    "drive": "FWD",
    "nameGerman": "1-Gang Automat Frontantrieb",
    "gearCount": 1
  }])
  .filter('geartypeCategory',function(gearTypes){
    return function(str){
      var cat = "";
      gearTypes.some(function(gt){
        if(gt.id == str){
          cat = gt.category;
          return true;
        }
      });
      return cat;
    };
  }).filter('geartypeNameGer',function(gearTypes){
    return function(str){
      var name = "";
      gearTypes.some(function(gt){
        if(gt.id == str){
          name = gt.nameGerman;
          return true;
        }
      });
      return name;
    };
  }).filter('saleType',function(gearTypes){
    return function(str){
      var typeCfg = {
        Demo : "Vorführwagen",
        Neu : "Neu",
        Occ : "Gebraucht"
      };
      return typeCfg[str];
    };
  });
