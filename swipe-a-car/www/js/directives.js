angular.module('starter.directives', [])

.directive('backImg', function() {
  return function(scope, element, attrs) {
    var url = attrs.backImg;
    element.css({
      'background-image': 'url(' + url + ')',
      'background-size': 'cover'
    });
  };
})

.filter('reverse', function() {
  return function(items) {
    return items.slice().reverse();
  };
});
