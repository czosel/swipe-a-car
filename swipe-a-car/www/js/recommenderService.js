angular.module('starter.recommenderService', [])

.factory('recommenderService', function (favoriteService, config, queryOperators, queryService) {

  var sigma = 0.5;
  var sigmaStepLength = 0.1;
  var maxMutationRecursions = 5;

  function getPreSelection(amountOfParents) {
    var selectedParents = [];
    list = favoriteService.getToplist().slice(0);
    for (var i = 0; i < amountOfParents; i++) {
      if (list.length === 0) break;
      var rKey = Math.floor(Math.random() * list.length);
      selectedParents.push(list[rKey]);
      list.splice(rKey, 1);
    }
    return selectedParents;
  }

  function recombination(father, mother) {
    return recombinationSplit(father, mother);
  }

  function recombinationRandom(father, mother){
    var child = {};
    Object.keys(config).forEach(function (attribute) {
      var r = Math.random();
      var value;
      if (r < 0.5) {
        value = mother[attribute];
      } else {
        value = father[attribute];
      }
      child[attribute] = value;
    });

    return getRealCarMutation(child);
  }

  function recombinationSplit(father, mother){
    var child = {};
    var splitBy = Math.floor((Math.random() * Object.keys(config).length));
    var currentNr = 0;
    Object.keys(config).forEach(function (attribute) {
      if (currentNr > splitBy) {
        value = mother[attribute];
      } else {
        value = father[attribute];
      }
      child[attribute] = value;
      currentNr++;
    });
    return getRealCarMutation(child);
  }

  function getRealCarMutation(unrealCar, sigmaOffset, recursionCount) {
    var searchQuery = {};

    var offset = (sigmaOffset === undefined) ? 0 : sigmaOffset;
    var recCount = (recursionCount === undefined) ? 0 : recursionCount;

    Object.keys(config).forEach(function (attribute) {
      var data = {
        factor: config[attribute].mutationFactor,
        curSigma: (sigma + offset),
        value: unrealCar[attribute]
      };

      var searchData = queryOperators[config[attribute].operator](data);
      if (searchData !== false) {
        searchQuery[attribute] = searchData;
      }
    });

    var results = queryService.fetchRowset(searchQuery);
    if (results.length === 0) {
      if(recCount == maxMutationRecursions){
        return queryService.getRandom();
      }

      return getRealCarMutation(unrealCar, offset + sigmaStepLength, recCount+1);
    } else {
      var randCarNr = Math.floor(Math.random() * results.length);
      if(favoriteService.isCarInToplist(results[randCarNr]) === true){
        if(recCount == maxMutationRecursions){
          return queryService.getRandom();
        }
        return getRealCarMutation(unrealCar, offset, recCount+1);
      }
      return results[randCarNr];
    }
  }


  return {
    getRecommendations: function (amount) {
      var selectedCars = getPreSelection(amount * 2);
      var recommendations = [];
      for (var i = 0; i < selectedCars.length; i += 2) {
        if (selectedCars.length > (i + 1)) {
          recommendations.push(recombination(selectedCars[i], selectedCars[i + 1]));
        }
      }
      return recommendations;
    },
    increaseSigma: function () {
      sigma += 0.1;
      if (sigma > 1) sigma = 1;
    },
    reduceSigma: function () {
      sigma -= 0.1;
      if (sigma < 0) sigma = 0;
    }
  };
});
